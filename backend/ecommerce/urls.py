from django.urls import include, path
from rest_framework import routers

from backend.ecommerce.api.viewsets import ProductViewSet

app_name = 'ecommerce'


router = routers.DefaultRouter()

router.register(r'products', ProductViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
