from django.db import models


class Product(models.Model):
    image = models.URLField(max_length=200)
    product_url = models.URLField(max_length=200)
    created_at = models.DateField()
    consult_date = models.DateField()
    vendas_no_dia = models.IntegerField(null=True, default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.image} {self.product_url} {self.created_at} {self.consult_date} {self.vendas_no_dia}"

    class Meta:
        verbose_name_plural = "Products"
