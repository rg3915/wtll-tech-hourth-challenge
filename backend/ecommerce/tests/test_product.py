from http import HTTPStatus

import requests


def test_get_products_raw_data():
    url = 'https://mc3nt37jj5.execute-api.sa-east-1.amazonaws.com/default/hourth_desafio'
    response = requests.get(url)
    assert response.status_code == HTTPStatus.OK


def test_get_products(client):
    '''
    Testa o retorno de uma lista de produtos agrupados por created_at.
    '''
    response = client.get('/api/v1/products/get_products/')
    assert response.status_code == HTTPStatus.OK
