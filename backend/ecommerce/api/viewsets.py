import requests
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from backend.ecommerce.models import Product

from .serializers import ProductSerializer


def group_by_product_url_and_created_at(data):
    result = {}
    for item in data:
        product_date = ','.join(
            (item['product_url'], item['product_url__created_at']))
        if product_date in result:
            result[product_date].append(item)
        else:
            result[product_date] = [item]
    return result


def get_json_result(result):
    _list = []
    for k, v in result.items():
        total = sum([item['vendas_no_dia'] for item in v])
        d = {}
        d['product_url__image'] = v[0]['product_url__image']
        d['product_url'] = v[0]['product_url']
        d['product_url__created_at'] = v[0]['product_url__created_at']
        d['total_sales'] = total
        d[v[0]['consult_date']] = v[0]['vendas_no_dia']
        _list.append(d)
    return _list


def get_products_raw_data():
    url = 'https://mc3nt37jj5.execute-api.sa-east-1.amazonaws.com/default/hourth_desafio'
    response = requests.get(url)
    return response.json()


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @action(detail=False, methods=['get'])
    def get_products(self, request, pk=None):
        data = get_products_raw_data()
        result = group_by_product_url_and_created_at(data)
        return Response({"result": get_json_result(result)})
