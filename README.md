# Wtll Tech Hourth challenge

## Este projeto foi feito com:

* [Python 3.10.4](https://www.python.org/)
* [Django 4.1.1](https://www.djangoproject.com/)
* [Django Rest Framework 3.14.0](https://www.django-rest-framework.org/)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/wtll-tech-hourth-challenge.git
cd wtll-tech-hourth-challenge

python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

python contrib/env_gen.py

python manage.py migrate
python manage.py createsuperuser --username="admin" --email=""

pytest
```

## Documentação

Entre nas páginas

http://localhost:8000/swagger/

ou

http://localhost:8000/redoc/

## Agrupamento dos produtos vendidos


O endpoint para retornar os produtos agrupados é

http://localhost:8000/api/v1/products/get_products/

